﻿function ManualFailover
{
       
                
        Write-Host
        
        $primary = Read-Host -Prompt "Primary SQL Instance(like server\instance)"
        Write-Host
        $dbName = Read-Host -Prompt "Database Name"
        Write-Host
        $loginName = Read-Host -Prompt "SQL Authenticated Login Name which already in Primary/Mirror Server"
        Write-Host
        $password = Read-Host -Prompt "Password of the above login"
        Write-Host
        

        Import-Module SQLPS -DisableNameChecking

        $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $primary

        $server.ConnectionContext.LoginSecure=$false
        $server.ConnectionContext.Set_login($loginName)
        $server.ConnectionContext.Set_Password($password)
        
        $dbmirroringstatus = $server.Databases | Where-Object {($_.IsMirroringEnabled -eq $true) -and ($_.name -eq $dbName)}
              
        if($dbmirroringstatus -ne $null)
        {
             $server.Databases[$dbName].ChangeMirroringState('Failover')
        }
        else
        {
            
            $dbexist = $server.Databases.Contains($dbName)

            If ($dbexist -eq $false)
            {
                Write-Host "Database: $dbName doesn't exist on the server:" + $server
                
            
            }
            else
            {
            
                Write-Host " Database not involved in Mirroring "

            }
            

        }

}
