﻿function ConfigureDatabaseMirroring
{
        
        Write-Host

        $primary = Read-Host -Prompt "Primary SQL Instance(like server\instance)"
        Write-Host
        $mirror = Read-Host -Prompt "Mirror SQL Instance(like server\instance)"
        Write-Host
        $sharePath = Read-Host -Prompt "Share Path(unc path like E:\backup\DB of Primary Server)"
        Write-Host
        $shareFolderName = Read-Host -Prompt "Share Folder name(like backup)"
        Write-Host
        $dbName = Read-Host -Prompt "Database Name"
        Write-Host
        $loginName = Read-Host -Prompt "SQL Authenticated Login Name which already in Primary/Mirror Server"
        Write-Host
        $password = Read-Host -Prompt "Password of the above login"
        Write-Host
        $tcpPort = Read-Host -Prompt "Port No"

        
        write-host
        write-host "============================================================="
        write-host " 1: Creating Shared Folder"
        write-host "============================================================="
        
        New-Item -ItemType directory -Path $sharePath

        New-SmbShare –Path $sharePath -Name $shareFolderName –FullAccess Everyone
        
        $shareName = "\\" + [System.Net.Dns]::GetHostByName($primary.NetName).HostName + "\" + $shareFolderName
        
        write-host
        write-host "============================================================="
        write-host " 2: Performing Initial checks"
        write-host "============================================================="

        PerformValidation $primary $mirror $shareName $dbName $loginName $password
        
        write-host
        write-host "============================================================="
        write-host " 3: Set Recovery Model as FULL on Primary Database"
        write-host "============================================================="
        $fullRecoveryModelType = [Microsoft.SqlServer.Management.Smo.RecoveryModel]::Full
        SetRecoveryModel $primary $dbName $fullRecoveryModelType $loginName $password

        write-host
        write-host "============================================================="
        write-host " 4: Perform Full Database Backup from Primary Server"
        write-host "============================================================="
        $backupActionType = [Microsoft.SqlServer.Management.Smo.BackupActionType]::Database
        $primaryBackupDataFile = Backup $primary $dbName $shareName $backupActionType $loginName $password

        
        write-host
        write-host "============================================================="
        write-host " 5: Restore Database backup on Mirror Server"
        write-host "============================================================="
        Restore $mirror $dbName $shareName $loginName $password

        write-host
        write-host "============================================================="
        write-host " 6: Create Endpoints for Database Mirroring"
        write-host "============================================================="

        $mirroringRole = [Microsoft.SqlServer.Management.Smo.ServerMirroringRole]::Partner
        $primaryFQName = GetFullyQualifiedMirroringEndpoint $primary $mirroringRole $tcpPort $loginName $password

        $mirrorFQName = GetFullyQualifiedMirroringEndpoint $mirror $mirroringRole $tcpPort $loginName $password

	    write-host $primaryFQName
	    write-host $mirrorFQName        


        write-host
        write-host "============================================================="
        write-host "  7: Set Primary/Mirror as Partner"
        write-host "============================================================="
        write-host "Connecting to Mirror and set Primary as partner ..."
        SetMirroringPartner $mirror $dbName $primaryFQName $loginName $password

        write-host "Connecting to Primary, set partner as mirror ..."
        SetMirroringPartner $primary $dbName $mirrorFQName $loginName $password
    
       
       
        
}

function PerformValidation
{
        Param([String]$primary , [String]$mirror, [String]$shareName, [String]$dbName,[String]$loginName,[String]$password)

        Import-Module SQLPS -DisableNameChecking
        $primaryServer = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $primary
        $mirrorServer = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $mirror

        $primaryServer.ConnectionContext.LoginSecure=$false
        $primaryServer.ConnectionContext.Set_login($loginName)
        $primaryServer.ConnectionContext.Set_Password($password)

        $mirrorServer.ConnectionContext.LoginSecure=$false
        $mirrorServer.ConnectionContext.Set_login($loginName)
        $mirrorServer.ConnectionContext.Set_Password($password)

        Write-Host "                                                                       "
    
        $db = $primaryServer.Databases.Contains($dbName)
        
        If ($db -eq $false)
        {
            $errorMessage = "Database:" + $dbName + " doesn't exist on Primary Server:" + $primaryServer
            throw $errorMessage
            
        }
        else
        {
            
            Write-Host "Database:$dbName is there on Primary Server:$primaryServer"

        }

        
        $dbmirroringstatus = $primaryServer.Databases | Where-Object {(($_.IsMirroringEnabled -eq $true -or $_.status -ne "Normal") -and $_.name -eq $dbName)} 

                
        if($dbmirroringstatus -ne $null)
        {
                
                Write-host " Either mirroring is already there for database:$dbname or Its not Online"
        }
        
        
        Write-Host "                                                                  "
        Write-Host "------------------------------------------------------------------"   
        Write-Host "                                                                  "    

        <#
        $mirrorDatabase = $mirrorServer.Databases.Contains($dbName)
        
        if($mirrorDatabase -eq $true)
        {
                $dbMeasures = $mirrorDatabase | measure-object
                if($dbMeasures.Count -ne 0)
                {
                        $errorMessage = "Database:" + $dbName + " already exists on mirror server:" + $mirror
                        throw $errorMessage
                }
        }
       

        Write-Host "Database $dbName doesn't exist on Mirror Server:$mirror"
        #>

        Write-Host "                                                                  "    

        
        if([System.IO.Directory]::Exists($shareName) -ne $True)
        {
                $errorMessage = "Share:" + $shareName + " does not exists"
                throw $errorMessage
        }
        
        write-host "File Share exist"
       
       
        
}

function SetRecoveryModel
{
       
        
        Param([String]$serverInstance, [String]$dbName, [String]$recoveryModel,[String]$loginName,[String]$password)

        Import-Module SQLPS -DisableNameChecking
        $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $serverInstance

        $server.ConnectionContext.LoginSecure=$false
        $server.ConnectionContext.Set_login($loginName)
        $server.ConnectionContext.Set_Password($password)

        $db = $server.Databases.Contains($dbName)
        
        If ($db -eq $false)
        {
            Write-Host "Database:"$dbName "doesn't exit on" $server
            
        }
        else
        {

            $server.Databases[$dbName].RecoveryModel = $recoveryModel

            $server.Databases[$dbName].Alter()

            Write-Host "Database:$dbName has been set to FULL Recovery Mode"
        
        }
              
        
}

Function Backup
{

    Param ([String]$instanceName, [String]$databasename, [String]$backupfolder,[String]$loginName,[String]$password)

    Import-Module SQLPS -DisableNameChecking
    
    $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $instanceName

    $server.ConnectionContext.LoginSecure=$false
    $server.ConnectionContext.Set_login($loginName)
    $server.ConnectionContext.Set_Password($password)

    
    $timestamp = Get-Date -format yyyyMMdd
    
    $backupfile = "$($databasename)_Full_$($timestamp).bak"
    $fullBackupFile = Join-Path $backupfolder $backupfile

    Backup-SqlDatabase -ServerInstance $instanceName -Database $databasename -BackupFile $fullBackupFile -Checksum -Initialize -BackupSetName "$databasename Full Backup" -CompressionOption On

    
    $timestamp = Get-Date -format yyyyMMdd
    
    $backupfile = "$($databasename)_Full_$($timestamp).bak"
    $txnBackupFile = Join-Path $backupfolder $backupfile

    Backup-SqlDatabase -BackupAction Log -ServerInstance $instanceName -Database $databasename -BackupFile $txnBackupFile

}

Function Restore
{

    Param ([String]$instanceName, [String]$originalDBName,[String]$backupfilefolder,[String]$loginName,[String]$password)

    Import-Module SQLPS -DisableNameChecking
    
    $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $instanceName

    $server.ConnectionContext.LoginSecure=$false
    $server.ConnectionContext.Set_login($loginName)
    $server.ConnectionContext.Set_Password($password)

    $fullBackupFile = Get-ChildItem $backupfilefolder | Sort-Object -Property LastWriteTime | Select-Object -Last 1
    
    
    $smoRestore = New-Object Microsoft.SqlServer.Management.Smo.Restore
    $smoRestore.Database = $originalDBName

    $smoRestore.Devices.AddDevice($fullBackupFile.FullName, [Microsoft.SqlServer.Management.Smo.DeviceType]::File)
    $smoRestore.NoRecovery = $TRUE

    $dbexistonmirror = $server.Databases.Contains($originalDBName)
     
    if ($dbexistonmirror -eq $true)
    {
        
         $smoRestore.ReplaceDatabase = $TRUE  
    } 
    else
    {
        $smoRestore.ReplaceDatabase = $FALSE

    }
    
    $filelist = $smoRestore.ReadFileList($server)
    
    
    $dataFolder = $server.Settings.DefaultFile;
    #Write-Host $dataFolder
    $logFolder = $server.Settings.DefaultLog;

    if ($dataFolder.Length -eq 0)
    {
        $dataFolder = $server.Information.MasterDBPath;
    }
    if ($logFolder.Length -eq 0) 
    {
        $logFolder = $server.Information.MasterDBLogPath;
    }


    $dataFileNumber = 0;

    foreach ($file in $filelist) 
    {
        $relocateFile = new-object 'Microsoft.SqlServer.Management.Smo.RelocateFile';
        $relocateFile.LogicalFileName = $file.LogicalName;
        $Lgf = $file.LogicalName
        #Write-Host $relocateFile.LogicalFileName = $file.LogicalName;
        if ($file.Type -eq 'D')
        {
            if($dataFileNumber -ge 1) { $suffix = "_$dataFileNumber"; }
            else { $suffix = $null; }
            $relocateFile.PhysicalFileName = "$dataFolder$Lgf.mdf";
            Write-Host $relocateFile.PhysicalFileName
            $dataFileNumber ++;
        }
        else 
        {
            $relocateFile.PhysicalFileName = "$logFolder$Lgf.ldf";
            Write-Host $relocateFile.PhysicalFileName
        }

        $smoRestore.RelocateFiles.Add($relocateFile) | out-null;
       


       
    }    

      
    $smoRestore.Action = [Microsoft.SqlServer.Management.Smo.RestoreActionType]::Database
    $smoRestore.SqlRestore($server)

    $smoRestore.NoRecovery = $True
    $smoRestore.Action = [Microsoft.SqlServer.Management.Smo.RestoreActionType]::Log
    $smoRestore.FileNumber = 2
    $smoRestore.SqlRestore($server)

}

function GetFullyQualifiedMirroringEndpoint
{
        Param([string]$serverInstance, [Microsoft.SqlServer.Management.Smo.ServerMirroringRole]$mirroringRole, [String]$tcpPort,[String]$loginName,[String]$password)

        Import-Module SQLPS -DisableNameChecking
        $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $serverInstance

        $server.ConnectionContext.LoginSecure=$false
        $server.ConnectionContext.Set_login($loginName)
        $server.ConnectionContext.Set_Password($password)

            
        $fullyQualifiedMirroringEndPointName = ""

        $AllEndPoints = $server.Endpoints.EnumEndpoints("DatabaseMirroring").EndpointType

        $AllEndPoints1 = $server.Endpoints.EnumEndpoints("DatabaseMirroring")
        
                 
        $EndPointList = @()
       
        foreach($endpoint in $AllEndPoints1)
        {
                $EndPointList += $endpoint.Protocol.Tcp
        }

        write-host $EndPointList
        
        
        if($AllEndPoints -eq $null)
        {
                $timestamp = Get-Date -format yyyyMMdd
                $endPointName = "Database_Mirroring_" + $timestamp
                write-host $endPointName
                $endpoint  = new-object ('Microsoft.SqlServer.Management.Smo.EndPoint')($server, $endPointName)
                $endpoint.ProtocolType = [Microsoft.SqlServer.Management.Smo.ProtocolType]::Tcp
                $endpoint.EndpointType = [Microsoft.SqlServer.Management.Smo.EndpointType]::DatabaseMirroring
                $endpoint.Protocol.Tcp.ListenerPort = $tcpPort
                $endpoint.Payload.DatabaseMirroring.ServerMirroringRole = $mirroringRole
                $endpoint.Create()
                $endpoint.Start()

                $endpoint = $server.Endpoints[$endPointName]
                #identify the Connect permission object
                $permissionSet = New-Object Microsoft.SqlServer.Management.Smo.ObjectPermissionSet([Microsoft.SqlServer.Management.Smo.ObjectPermission]::Connect)
                #grant permission
                $endpoint.Grant($permissionSet,$loginName)

        
                # TCP:Server:port
                $fullyQualifiedMirroringEndPointName = "TCP://" + [System.Net.Dns]::GetHostByName($server.NetName).HostName + ":" + $tcpPort
                   
         }
         else
         {
             foreach($endPoint in $EndPointList)
             {
                  $fullyQualifiedMirroringEndPointName = "TCP://" + [System.Net.Dns]::GetHostByName($server.NetName).HostName + ":" + $endPoint.Properties["ListenerPort"].Value
                  break
             }
         }
        
        write-host "Server Name:$serverInstance"
        write-host "Mirroring Role:$mirroringRole"
        write-host "EndPointName:$fullyQualifiedMirroringEndPointName"
        $fullyQualifiedMirroringEndPointName
        
}

function SetMirroringPartner
{
        Param([String]$serverInstance,[String]$dbName,[string]$fqName,[String]$loginName,[String]$password)

        Import-Module SQLPS -DisableNameChecking
        <#
        $server = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $serverInstance
        write-host $server

        $server.ConnectionContext.LoginSecure=$false
        $server.ConnectionContext.Set_login($loginName)
        $server.ConnectionContext.Set_Password($password)
                       
        $db = New-Object Microsoft.SqlServer.Management.Smo.Database($server,$dbName)

        
        $db.MirroringPartner = $fqName

        $db.Alter()
        #>
            
        $conn = "Server=$serverInstance; Integrated Security=false; Database= master; user id= $loginName; password= $password"

        $cn = New-Object "System.Data.SqlClient.SqlConnection" $conn
        $cn.Open()
        $cmd = New-Object "System.Data.SqlClient.SqlCommand"
        $cmd.CommandType = [System.Data.CommandType]::Text
 
        $cmd.CommandText = "ALTER DATABASE $dbName SET PARTNER = '$fqName'"
        $cmd.Connection = $cn
        $cmd.ExecuteNonQuery()
        $cn.Close()
}